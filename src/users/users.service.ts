import { Injectable, Inject } from '@nestjs/common';
import { User } from './users.entity';

@Injectable()
export class UsersService {
  constructor(
    @Inject('USERS_REPOSITORY')
    private usersRepository: typeof User,
  ) {}

  async getUsers(): Promise<User[]> {
    return this.usersRepository.findAll<User>();
  }
}
