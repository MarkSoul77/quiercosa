import { databaseConfigInterface } from './interfaces/databaseConfig';

export const databaseConfig: databaseConfigInterface = {
  development: {
    username: process.env.MYSQL_USER_DEVELOP,
    password: process.env.MYSQL_PASSWORD_DEVELOP,
    database: process.env.MYSQL_DATABASE_DEVELOP,
    host: process.env.MYSQL_HOST_DEVELOP,
    port: process.env.MYSQL_PORT_DEVELOP,
    dialect: process.env.MYSQL_DIALECT,
  },
  production: {
    username: process.env.MYSQL_USER_PRODUCTION,
    password: process.env.MYSQL_PASSWORD_PRODUCTION,
    database: process.env.MYSQL_DATABASE_PRODUCTION,
    host: process.env.MYSQL_HOST_PRODUCTION,
    port: process.env.MYSQL_PORT_PRODUCTION,
    dialect: process.env.MYSQL_DIALECT,
  },
};
