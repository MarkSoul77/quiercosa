export interface databaseConfigInterface {
  development: {
    username: string;
    password: string;
    database: string;
    host: string;
    port: string;
    dialect: string;
  };
  production: {
    username: string;
    password: string;
    database: string;
    host: string;
    port: string;
    dialect: string;
  };
}
