import { Sequelize } from 'sequelize-typescript';
import { databaseConfig } from './database.config';
import { databaseConstants } from '../constansts';
import { User } from 'src/users/users.entity';

export const databaseProviders = [
  {
    provide: 'SEQUELIZE',
    useFactory: async () => {
      let config;
      switch (process.env.NODE_ENV as any) {
        case databaseConstants.DEVELOPMENT:
          config = databaseConfig.development;
          break;
        case databaseConstants.PRODUCTION:
          config = databaseConfig.production;
          break;
        default:
          config = databaseConfig.development;
      }
      const sequelize = new Sequelize(config);
      sequelize.addModels([User]);
      await sequelize.sync();
      return sequelize;
    },
  },
];
