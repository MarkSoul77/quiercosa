#En qué se va a basar la imagen de linux
FROM node:18.14.2

#Qué directorio va a utilizar dentro de nuestro contenedor
WORKDIR /app

#Copiar todo lo que esté en el directorio de Dockerfile a nuestro contenedor
COPY . /app

#Exponer el puerto para poder acceder al contenedor
EXPOSE 3000

#Cuando inicia el contenedor que es lo que se debe ejecutar al inicio.
ENTRYPOINT npm run start:dev
#CMD ["start:dev"]